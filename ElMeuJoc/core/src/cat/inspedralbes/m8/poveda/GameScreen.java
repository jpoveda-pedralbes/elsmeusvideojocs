package cat.inspedralbes.m8.poveda;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;

//Si no vull tenir molts metodes buits per ahi puc heretar de ScreenAdapter en compte de Screen
public class GameScreen extends ScreenAdapter {
	
	//Referencia
	PovedaPereiraGame game;
	
	// Coses propies de la Screen
	// Model de dades
	Rectangle player;
	List<Rectangle> enemies;
	List<Rectangle> playerShoots;
	
	Texture playerTexture;
	Texture enemyTexture;
	Texture shootTexture;
	
	float lastEnemyTime = 0;
	int screenWidth;
	int screenHeight;
	
	public GameScreen(PovedaPereiraGame game) {
		this.game = game;
		
		// Configurem amb la resolucio del nostre joc
		//camera = new OrthographicCamera();
		//camera.setToOrtho(false, screenWidth, screenHeight);
		
		player 		  = new Rectangle(0, 0, Constants.PLAYER_WIDTH, Constants.PLAYER_HEIGTH);
		enemies 	  = new ArrayList<Rectangle>();
		playerShoots  = new ArrayList<Rectangle>();
		
		playerTexture = new Texture(Gdx.files.internal("naus/player.png"));
		enemyTexture  = new Texture(Gdx.files.internal("naus/enemy.png"));
		shootTexture  = new Texture(Gdx.files.internal("lasers/laserBlue.png"));
		
		screenWidth   = Gdx.graphics.getWidth();
		screenHeight  = Gdx.graphics.getHeight();
		
		game.playerScore = 0;
	}
	
	@Override
	public void render (float delta) {

		gestionarInput();
		
		actualitza();
		
		// 3. Dibuixat
		
		// Les dues seguents linies son molt important perque la classe que dibuixa i la
		// camera han de tenir la mateixa idea
		// de la geometria de les coses del joc. Mes endavant veure mes sentit a aixo
		//camera.update();
		//batch.setProjectionMatrix(camera.combined);
				
		game.batch.begin();
		
		//Components grafics del videojoc
		game.batch.draw(playerTexture, player.x, player.y, player.width, player.height);
		for (Rectangle enemy : enemies) {
			game.batch.draw(enemyTexture, enemy.x, enemy.y, enemy.width, enemy.height);
		}	
		for (Rectangle shoot : playerShoots) {
			game.batch.draw(shootTexture, shoot.x, shoot.y, shoot.width, shoot.height);
		}
		game.batch.end();
	}
	
	/**
	 * Metodo privado para hacer aparecer los enemigos segun el tiempo que pase y a una altura random
	 */
	private void actualitza(){
		float delta = Gdx.graphics.getDeltaTime();
		
		lastEnemyTime += delta;
		if( lastEnemyTime > Constants.TIME_BETWEEN_ENEMIES ){
			int x = new Random().nextInt(this.screenWidth);
			//TODO: Genera una y aleatoria -> DONE
			int y = this.screenHeight;
			
			Rectangle newEnemy = new Rectangle( x, y, 100, 100 );
			enemies.add(newEnemy);
			
			lastEnemyTime = 0;
		}
		
		for (Rectangle shoot : playerShoots) {
			//volem que els dispars vagin cap amunt. Per tant sumem a la y
			shoot.y += Constants.PLAYER_SHOOT_SPEED * delta;
		}
		
		for( Rectangle enemy : enemies){
			enemy.y -= Constants.ENEMY_SPEED*delta;
		}
		
		for (Rectangle enemy : enemies) {
			if(enemy.overlaps(player)) {
				game.setScreen(new GameOverScreen(game));
			}
		}
		
		for (Iterator<Rectangle> iteratorEnemies = enemies.iterator(); iteratorEnemies.hasNext();) {
			Rectangle enemy = (Rectangle) iteratorEnemies.next();
			for (Iterator<Rectangle> iteratorShoot = playerShoots.iterator(); iteratorShoot.hasNext();) {
				Rectangle shoot = (Rectangle) iteratorShoot.next();

				//Si l'enemic que estem mirant xoca amb el dispar que estem mirant volem eliminar-los als dos
				if(shoot.overlaps(enemy)) {
					iteratorShoot.remove();
					iteratorEnemies.remove();
					game.playerScore+=5;
					break;
				}
			}
		}
		
		for( Iterator<Rectangle> iterator = enemies.iterator(); iterator.hasNext();) {
			//TODO: si es detecta que l'enemic surt de la pantalla, s'esborra de la llista amb l'iterator -> DONE
			Rectangle enemy = (Rectangle) iterator.next();
			if( enemy.y < 0 ){
				iterator.remove();
			}
		}
	}
	
	/**
	 * Metodo privado para gestionar las teclas que el usuario aprieta
	 */
	private void gestionarInput(){
		
		float delta = Gdx.graphics.getDeltaTime();
		
		//MOVIMENT
		if( Gdx.input.isKeyPressed(Keys.LEFT) ){
			player.x -= Constants.PLAYER_SPEED*delta;
		}
		if( Gdx.input.isKeyPressed(Keys.RIGHT) ){
			player.x += Constants.PLAYER_SPEED*delta;
		}
		if( Gdx.input.isKeyPressed(Keys.DOWN) ){
			player.y -= Constants.PLAYER_SPEED*delta;
		}
		if( Gdx.input.isKeyPressed(Keys.UP) ){
			player.y += Constants.PLAYER_SPEED*delta;
		}
		
		if (Gdx.input.isKeyJustPressed(Keys.SPACE)) {
			float x= player.x + player.width/2;
			float y = player.y + player.height;  
			playerShoots.add(new Rectangle(x, y, Constants.LASER_WIDTH, Constants.LASER_HEIGTH));
		}
		
		//LIMITS PANTALLA
		if( player.x < 0 ) {
			player.x = 0;
		}
		if( player.x > (this.screenWidth-player.width) ) {
			player.x = this.screenWidth-player.width;
		}
		if( player.y < 0 ) {
			player.y = 0;
		}
		if( player.y > (this.screenHeight-player.height) ) {
			player.y = this.screenHeight-player.height;
		}
	}
	
	@Override
	public void dispose () {
		game.batch.dispose();
		playerTexture.dispose();
		enemyTexture.dispose();
		shootTexture.dispose();
	}
}
