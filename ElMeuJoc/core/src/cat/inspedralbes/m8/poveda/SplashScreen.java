package cat.inspedralbes.m8.poveda;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;

public class SplashScreen implements Screen{

	// Referencia a joc
	PovedaPereiraGame game;
	
	// Coses propies de la Screen
	// Imatge de l'empresa
	Texture logo;
	// Variables per a saber si han passat els 3 segons
	float tempsDesdeIniciJoc = 0;
	static int TEMPS_SPLASHSCREEN_S = 3;
	
	public SplashScreen(PovedaPereiraGame game) {
		this.game = game;
		logo = new Texture(Gdx.files.internal("badlogic.jpg"));
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(float delta) {
		tempsDesdeIniciJoc += delta;
		// Si ha passat un temps, canvio a la screen del joc.
		if (tempsDesdeIniciJoc > TEMPS_SPLASHSCREEN_S) {

			// Quan ha passat el temps vull canviar
			// Creo objecte i li dono referencia
			game.setScreen(new GameScreen(game));

		} else {

			// camera.update();
			// batch.setProjectionMatrix(camera.combined);

			// dibuixo LOGO
			int ample = Gdx.graphics.getWidth();
			int alt = Gdx.graphics.getHeight();
			// Dibuixo estirant a tota la pantalla
			// He de preguntar per la mida
			game.batch.begin();
			game.batch.draw(logo, 0, 0, ample, alt);
			game.batch.end();
		}
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() { 
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		logo.dispose();
	}
	
}
