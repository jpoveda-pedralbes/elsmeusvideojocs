package cat.inspedralbes.m8.poveda;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * ElMeuJoc / M8-UF3
 * @author Javi Poveda
 *
 */

public class PovedaPereiraGame extends Game {
		
	SpriteBatch batch;
	BitmapFont font;
	OrthographicCamera camera;
	int playerScore;
	
	@Override
	public void create () {
		
		batch = new SpriteBatch();
		font = new BitmapFont();
		//Nomes engegar el joc anem a la screen del logo
		//Passem la ref a la screen
		setScreen(new SplashScreen(this));
		
		Gdx.app.setLogLevel(Application.LOG_DEBUG);
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		// Aquest metode crida al render del pare (classe Game) i aquest crida al render
		// de la screen activa. sense aques super.render no es veura res.
		super.render();
	}
	
	@Override
	public void dispose() {
		batch.dispose();
	}
	
}
