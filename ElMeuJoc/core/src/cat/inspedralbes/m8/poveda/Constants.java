package cat.inspedralbes.m8.poveda;

public class Constants {
	public static final int TIME_BETWEEN_ENEMIES = 2;
	public static final int PLAYER_WIDTH = 95;
	public static final int PLAYER_HEIGTH = 95;
	public static final int PLAYER_SPEED = 300; // Pixels per segons
	public static final int ENEMY_SPEED = 150; // Pixels per segons
	public static final float LASER_WIDTH = 20/3;
	public static final float LASER_HEIGTH = 100/3;
	public static final float PLAYER_SHOOT_SPEED = 300;
}
