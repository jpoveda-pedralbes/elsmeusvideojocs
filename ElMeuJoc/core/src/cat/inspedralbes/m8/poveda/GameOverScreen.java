package cat.inspedralbes.m8.poveda;

import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;

public class GameOverScreen implements Screen {
	
	// Referencia a joc
	PovedaPereiraGame game;
	
	// Coses propies de la Screen
	// Imatge de l'empresa
	Texture logo;
	
	// Variables per a saber si han passat els 3 segons
	public GameOverScreen(PovedaPereiraGame game) {
		this.game = game;
		logo = new Texture(Gdx.files.internal("gameover.png"));
	}
	
	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(float delta) {
		// dibuixo LOGO
		int ample  = Gdx.graphics.getWidth();
		int alt    = Gdx.graphics.getHeight()/2;
		int y_menu = alt-20;
		
		String [] opcionsMenu = {
			"Puntuaci� del jugador > "+game.playerScore,
			"------------------------------------------------------------------------",
			"Prem espai o fes click per continuar jugant (Space/Click)", 
			"Prem escape per sortir del joc (ESC)"
		}; 
		
		// Dibuixo estirant a tota la pantalla
		// He de preguntar per la mida
		game.batch.begin();
		
		game.batch.draw(logo, 0, alt, ample, alt);
		
		for( String i : opcionsMenu ) {
			game.font.draw(game.batch, i, 20, y_menu);
			y_menu-=25;
		}
		
		if( Gdx.input.isKeyPressed(Keys.SPACE) || (Gdx.input.isButtonPressed(Buttons.LEFT) || Gdx.input.isButtonPressed(Buttons.RIGHT)) ){
			game.setScreen(new SplashScreen(game));
		}
		if( Gdx.input.isKeyPressed(Keys.ESCAPE) ){
			Gdx.app.exit();
		}
		
		game.batch.end();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		logo.dispose();
	}

}
