package cat.inspedralbes.m8.poveda.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import cat.inspedralbes.m8.poveda.PovedaPereiraGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Joc de naus";
		config.width = 400; 
		config.height = 800;
		config.fullscreen=false;
		new LwjglApplication(new PovedaPereiraGame(), config);
	}
}
